import random

from quicksort import quickSort

def test_quicksort():
    my_list = list(range(0, 80000))
    random.shuffle(my_list)
    quickSort(my_list, 0, len(my_list) - 1)
    assert (my_list == sorted(my_list))

