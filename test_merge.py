from app import mergeSort
import random

def test_selection():
    my_list = list(range(0, 80000))
    random.shuffle(my_list)
    mergeSort(my_list)
    assert (my_list == sorted(my_list))
