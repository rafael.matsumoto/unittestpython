import random
from app import heapSort

def test_heap():
    my_list = list(range(0, 80000))
    random.shuffle(my_list)
    heapSort(my_list)
    assert (my_list == sorted(my_list))

