import random

from app import insertionSort


def test_insertion():
    my_list = list(range(0, 80000))
    random.shuffle(my_list)
    insertionSort(my_list)
    assert (my_list == sorted(my_list))
