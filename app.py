import random

def selection_sort(param_list):
    for i in range(0, len(param_list)):
        minimum = i
        for j in range(i + 1, len(param_list)):
            if param_list[j] < param_list[minimum]:
                minimum = j
        temp = param_list[i]
        param_list[i] = param_list[minimum]
        param_list[minimum] = temp
    return param_list


def insertionSort(param_list):
   for index in range(1, len(param_list)):

        currentvalue = param_list[index]
        position = index

        while position>0 and param_list[position - 1]>currentvalue:
             param_list[position]=param_list[position - 1]
             position = position-1

        param_list[position]=currentvalue

   return  param_list


def mergeSort(arr):
    if len(arr) > 1:
        mid = len(arr) // 2  # Finding the mid of the array
        L = arr[:mid]  # Dividing the array elements
        R = arr[mid:]  # into 2 halves

        mergeSort(L)  # Sorting the first half
        mergeSort(R)  # Sorting the second half

        i = j = k = 0

        # Copy data to temp arrays L[] and R[]
        while i < len(L) and j < len(R):
            if L[i] < R[j]:
                arr[k] = L[i]
                i += 1
            else:
                arr[k] = R[j]
                j += 1
            k += 1

        # Checking if any element was left
        while i < len(L):
            arr[k] = L[i]
            i += 1
            k += 1

        while j < len(R):
            arr[k] = R[j]
            j += 1
            k += 1
    return arr


def partition(param_list, start, end):
    pivot = param_list[end]
    bottom = start - 1
    top = end

    done = 0
    while not done:

        while not done:
            bottom = bottom + 1

            if bottom == top:
                done = 1
                break

            if param_list[bottom] > pivot:
                param_list[top] = param_list[bottom]
                break

        while not done:
            top = top - 1

            if top == bottom:
                done = 1
                break

            if param_list[top] < pivot:
                param_list[bottom] = param_list[top]
                break

    param_list[top] = pivot
    return top


def quicksort(param_list, start, end):
    if start < end:
        split = partition(param_list, start, end)
        quicksort(param_list, start, split - 1)
        quicksort(param_list, split + 1, end)
    else:
        return

    return param_list


# Python3 program for implementation of Shell Sort

def shellSort(arr):
    # Start with a big gap, then reduce the gap
    n = len(arr)
    gap = n // 2

    # Do a gapped insertion sort for this gap size.
    # The first gap elements a[0..gap-1] are already in gapped
    # order keep adding one more element until the entire array
    # is gap sorted
    while gap > 0:

        for i in range(gap, n):

            # add a[i] to the elements that have been gap sorted
            # save a[i] in temp and make a hole at position i
            temp = arr[i]

            # shift earlier gap-sorted elements up until the correct
            # location for a[i] is found
            j = i
            while j >= gap and arr[j - gap] > temp:
                arr[j] = arr[j - gap]
                j -= gap

            # put temp (the original a[i]) in its correct location
            arr[j] = temp
        gap //= 2


# return arr

def bubbleSort(param_list):
    for passnum in range(len(param_list) - 1, 0, -1):
        for i in range(passnum):
            if param_list[i] > param_list[i + 1]:
                temp = param_list[i]
                param_list[i] = param_list[i + 1]
                param_list[i + 1] = temp
    return param_list

def heapify(arr, n, i):
    largest = i # Initialize largest as root
    l = 2 * i + 1     # left = 2*i + 1
    r = 2 * i + 2     # right = 2*i + 2

    # See if left child of root exists and is
    # greater than root
    if l < n and arr[i] < arr[l]:
        largest = l

    # See if right child of root exists and is
    # greater than root
    if r < n and arr[largest] < arr[r]:
        largest = r

    # Change root, if needed
    if largest != i:
        arr[i],arr[largest] = arr[largest],arr[i] # swap

        # Heapify the root.
        heapify(arr, n, largest)

# The main function to sort an array of given size
def heapSort(arr):
    n = len(arr)

    # Build a maxheap.
    for i in range(n, -1, -1):
        heapify(arr, n, i)

    # One by one extract elements
    for i in range(n-1, 0, -1):
        arr[i], arr[0] = arr[0], arr[i] # swap
        heapify(arr, i, 0)

