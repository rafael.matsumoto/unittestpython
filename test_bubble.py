import app
import random

def test_bubble():
    my_list = list(range(0, 80000))
    random.shuffle(my_list)
    app.bubbleSort(my_list)
    assert (my_list == sorted(my_list))

