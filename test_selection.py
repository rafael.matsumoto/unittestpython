from app import selection_sort
import random

def test_selection():
    my_list = list(range(0, 80000))
    random.shuffle(my_list)
    selection_sort(my_list)
    assert (my_list == sorted(my_list))

